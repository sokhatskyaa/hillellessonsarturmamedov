package hillel.hw1.sokhatskiy.com;

public class Human {
    private String name;
    private String surname;
    private String patromic;
    private String FullName;

    public Human (String name, String surname) {
        this.name=name;
        this.surname=surname;
    }

    public Human (String name, String surname, String patromic) {
        this.name=name;
        this.surname=surname;
        this.patromic=patromic;
    }

    public Human (String FullName) {
        this.FullName=FullName;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatromic(){
        return patromic;
    }

    public String getFullName() {
        return FullName;
    }
}
